const tintColor = "#F76C1D";

export default {
  tintColor,
  tabIconDefault: "#F76C1D",
  tabIconSelected: tintColor,
  tabBar: "#F76C1D",
  errorBackground: "red",
  errorText: "#F76C1D",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#F76C1D"
};

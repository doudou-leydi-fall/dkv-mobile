import React from "react";
import { Platform } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";

import TabBarIcon from "../components/TabBarIcon";
import EventHomeScreen from "../screens/Event/HomeScreen";
import PlaceFoodScreen from "../screens/Food/PlaceDetailsScreen";
import ListPage from "../screens/Event/_views/List";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {}
});

const HomeStack = createStackNavigator(
  {
    Home: EventHomeScreen
  },
  {
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);

HomeStack.navigationOptions = {
  tabBarLabel: "Évènement",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === "ios"
          ? `ios-information-circle${focused ? "" : "-outline"}`
          : "md-information-circle"
      }
    />
  ),
  tabBarOptions: {
    activeTintColor: "#3B1F6F",
    labelStyle: {
      fontSize: 12,
      fontWeight: "700"
    },
    style: {
      backgroundColor: "#50E3C2",
      color: "#F76D1E",
      fontWeight: "700"
    }
  }
};

HomeStack.path = "";

const PlaceFoodStack = createStackNavigator(
  {
    Food: PlaceFoodScreen
  },
  {
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#f4511e"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);

PlaceFoodStack.navigationOptions = {
  tabBarLabel: "Food",
  navigationOptions: {
    header: null
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-link" : "md-link"}
    />
  )
};

PlaceFoodStack.path = "";

const ListStack = createStackNavigator(
  {
    List: ListPage
  },
  {
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#f4511e"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);

ListStack.navigationOptions = {
  tabBarLabel: "List",
  navigationOptions: {
    header: null
  },
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-link" : "md-link"}
    />
  )
};

ListStack.path = "";

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  PlaceFoodStack,
  ListStack
});

tabNavigator.path = "";

export default tabNavigator;

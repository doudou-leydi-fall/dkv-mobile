/* eslint-enable import/no-unresolved, import/extensions */

export const getPlatformElevation = elevation => {
  if (elevation === 0) {
    return {
      shadowColor: "transparent",
      zIndex: 0
    };
  }

  return {
    // we need to have zIndex on iOS, otherwise the shadow is under components that
    // are rendered later
  };
};

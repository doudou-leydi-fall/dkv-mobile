import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from "react-native";

const { width } = Dimensions.get("window");

export const Section = ({ title, items, childrenComponent, titleStyle }) => (
  <View style={sectionStyle.container}>
    <View style={sectionStyle.titleContainer}>
      <Text style={titleStyle || sectionStyle.title}>{title}</Text>
    </View>
    <View style={sectionStyle.body}>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {items.map((data, key) =>
          React.cloneElement(childrenComponent, {
            key: key,
            width: width - 70,
            data: data
          })
        )}
      </ScrollView>
    </View>
  </View>
);

const sectionStyle = StyleSheet.create({
  container: {
    paddingRight: 10
  },
  titleContainer: {
    paddingLeft: 20
  },
  title: {
    fontSize: 24,
    color: "#FFF",
    fontWeight: "700"
  }
});

import React from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions
} from "react-native";
import { Avatar, Button, ListItem } from "react-native-elements";

const { width } = Dimensions.get("window");

export const CardList = () => (
  <View
    style={{
      display: "flex",
      flexDirection: "column",
      marginRight: 20,
      marginLeft: 20,
      marginBottom: 20,
      marginTop: 20,
      width: width - 40 || 330
    }}
  >
    <View style={ThumbnailWithListStyle.header}>
      <View style={ThumbnailWithListStyle.artistinfo}>
        <Avatar
          rounded
          size="large"
          source={{
            uri:
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQez_CSxdqEDAg6N16KmAe1Cs4HDw_Z_rl_UhWChu0XO3zQqHux"
          }}
        />
        <Text style={ThumbnailWithListStyle.name}>Waly Seck</Text>
      </View>
      <Button
        title="S'abonner"
        containerStyle={ThumbnailWithListStyle.btnFollowContainer}
        buttonStyle={ThumbnailWithListStyle.btnFollow}
        titleStyle={ThumbnailWithListStyle.btnFollowLabel}
        color={"#FFF"}
      />
    </View>
    <View style={ThumbnailWithListStyle.listContainer}>
      <View style={ThumbnailWithListStyle.listItem}>
        <Text style={ThumbnailWithListStyle.listItemRightLabel}>
          Concert Sorano
        </Text>
        <Text style={ThumbnailWithListStyle.listItemLeftLabel}>
          Lundi 22 juin 2019
        </Text>
      </View>
      <View style={ThumbnailWithListStyle.listItem}>
        <Text style={ThumbnailWithListStyle.listItemRightLabel}>
          Concert Sorano
        </Text>
        <Text style={ThumbnailWithListStyle.listItemLeftLabel}>
          Lundi 22 juin 2019
        </Text>
      </View>
      <View style={ThumbnailWithListStyle.listItem}>
        <Text style={ThumbnailWithListStyle.listItemRightLabel}>
          Concert Sorano
        </Text>
        <Text style={ThumbnailWithListStyle.listItemLeftLabel}>
          Lundi 22 juin 2019
        </Text>
      </View>
      <Button
        title="Voir tout"
        type="outline"
        containerStyle={ThumbnailWithListStyle.btnBottom}
        titleStyle={ThumbnailWithListStyle.btnDetailsLabel}
      />
    </View>
  </View>
);

const ThumbnailWithListStyle = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  listContainer: {
    width: width - 70
  },
  artistinfo: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  name: {
    color: "#F76D1E",
    fontSize: 18,
    marginLeft: 10,
    fontWeight: "600"
  },
  btnFollow: {
    backgroundColor: "#F76D1E",
    borderRadius: 20,
    paddingRight: 20,
    paddingLeft: 20
  },
  btnFollowContainer: {
    height: 40
  },
  btnFollowLabel: {
    fontSize: 14
  },
  leftContent: {
    color: "#FFF"
  },
  listItemContainer: {
    backgroundColor: "transparent",
    borderBottomWidth: 1,
    borderBottomColor: "#FFF"
  },
  rightTitle: {
    color: "#50E3C2",
    fontWeight: "600",
    fontSize: 12,
    width: 120
  },
  listItem: {
    height: 45,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#FFF"
  },
  listItemRightLabel: {
    color: "#FFF",
    fontWeight: "600"
  },
  listItemLeftLabel: {
    color: "#50E3C2",
    fontSize: 14
  },
  btnBottom: {
    backgroundColor: "transparent",
    borderWidth: 2,
    borderRadius: 4,
    borderColor: "#FFF",
    marginTop: 30
  },
  btnDetailsLabel: {
    color: "#FFF",
    fontSize: 15,
    margin: 3
  }
});

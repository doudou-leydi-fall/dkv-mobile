import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  Dimensions,
  StyleSheet,
  Text,
  View
} from "react-native";

const { width } = Dimensions.get("window");

export const WidthCard = ({
  footerLT,
  footerLB,
  footerRT,
  footerLTStyle,
  footerLBStyle,
  footerRB,
  footerRTStyle,
  footerRBStyle,
  src,
  height
}) => {
  return (
    <View
      style={{
        minHeight: height || 240,
        width: width - 40 || 330,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 0,
        overflow: "hidden"
      }}
    >
      <View style={panelStyle.cardImageContainer}>
        <Image
          style={panelStyle.cardImage}
          source={{
            uri: src
          }}
        />
      </View>
      <View style={panelStyle.cardFooterContainer}>
        <View style={panelStyle.footerL}>
          <Text style={footerLTStyle || panelStyle.footerLT}>{footerLT}</Text>
          <Text style={footerLBStyle || panelStyle.footerLB}>{footerLB}</Text>
        </View>
        <View style={panelStyle.footerR}>
          <Text style={footerRTStyle || panelStyle.footerRT}>{footerRT}</Text>
          <Text style={footerRBStyle || panelStyle.footerRB}>{footerRB}</Text>
        </View>
      </View>
    </View>
  );
};

const panelStyle = StyleSheet.create({
  cardImageContainer: {
    height: 180,
    borderRadius: 5,
    overflow: "hidden"
  },
  cardImage: {
    width: "100%",
    height: 200,
    borderRadius: 5,
    resizeMode: "cover"
  },
  cardFooterContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginTop: 10
  },
  cardTitle: { paddingTop: 10 },
  footerL: {
    width: "60%"
  },
  footerLT: {
    fontSize: 17,
    color: "#FFF",
    fontWeight: "600",
    paddingBottom: 2
  },
  footerLB: {
    fontSize: 13,
    color: "#FFF"
  },
  footerR: {
    display: "flex",
    justifyContent: "flex-end",
    width: "40%"
  },
  footerRT: {
    fontSize: 13,
    color: "#50E3C2",
    marginBottom: 3,
    textAlign: "right"
  },
  footerRB: {
    fontSize: 18,
    fontWeight: "600",
    color: "#F76D1E",
    textAlign: "right"
  }
});

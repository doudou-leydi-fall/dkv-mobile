import React from "react";
import {
  Image,
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View
} from "react-native";
import { Icon } from "react-native-elements";

const { width, height } = Dimensions.get("window");

export const SlimCard = ({ id, label, notation, src }) => (
  <View
    style={{
      minHeight: 240,
      width: width / 2 - 50 || 130,
      margin: 20
    }}
  >
    <View style={panelStyle.cardImageContainer}>
      <Image
        style={panelStyle.cardImage}
        source={{
          uri: src
        }}
      />
    </View>
    <View style={panelStyle.cardHeaderContainer}>
      <Text style={panelStyle.label}>{label}</Text>
      <View style={panelStyle.notationContainer}>
        <Text style={panelStyle.notation}>{notation}</Text>
        <Icon name="star" size={18} color="#F8E71C" />
      </View>
    </View>
  </View>
);

const panelStyle = StyleSheet.create({
  cardImageContainer: {
    height: 220,
    borderRadius: 5,
    overflow: "hidden"
  },
  cardImage: {
    width: "100%",
    height: 220,
    borderRadius: 5,
    resizeMode: "cover"
  },
  cardHeaderContainer: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginTop: 10
  },
  cardTitle: { paddingTop: 10 },
  label: {
    width: "80%",
    fontSize: 14,
    color: "#FFF",
    fontWeight: "600",
    paddingBottom: 2
  },
  headerLB: {
    fontSize: 13,
    color: "#FFF"
  },
  headerR: {
    display: "flex",
    justifyContent: "flex-end"
  },
  headerRT: {
    fontSize: 13,
    color: "#50E3C2",
    marginBottom: 3
  },
  headerRB: {
    fontSize: 18,
    fontWeight: "600",
    color: "#50E3C2",
    textAlign: "right"
  },
  notationContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  notation: {
    color: "#F8E71C"
  }
});

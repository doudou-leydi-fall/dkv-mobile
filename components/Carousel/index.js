import React, { PureComponent } from "react";
import {
  Animated,
  View,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView
} from "react-native";
const logo = require("../../assets/images/logo-official.png");
const deviceWidth = Dimensions.get("window").width;
const FIXED_BAR_WIDTH = 280;
const BAR_SPACE = 10;
const images = [
  "https://i.ytimg.com/vi/2Q0K1FqetgY/maxresdefault.jpg",
  "https://s-media-cache-ak0.pinimg.com/originals/40/4f/83/404f83e93175630e77bc29b3fe727cbe.jpg",
  "https://s-media-cache-ak0.pinimg.com/originals/8d/1a/da/8d1adab145a2d606c85e339873b9bb0e.jpg"
];

export class Carousel extends PureComponent {
  numItems = this.props.pictures && this.props.pictures.length;
  itemWidth = FIXED_BAR_WIDTH / this.numItems - (this.numItems - 1) * BAR_SPACE;
  animVal = new Animated.Value(0);

  render() {
    console.log("pictures log", this.props.pictures);
    let imageArray = [];
    let barArray = [];
    this.props.pictures &&
      this.props.pictures.map((image, i) => {
        console.log(image, i);
        const thisImage = (
          <Image
            key={`image${i}`}
            source={{ uri: image }}
            style={{ width: deviceWidth }}
          />
        );
        imageArray.push(thisImage);

        const scrollBarVal = this.animVal.interpolate({
          inputRange: [deviceWidth * (i - 1), deviceWidth * (i + 1)],
          outputRange: [-this.itemWidth, this.itemWidth],
          extrapolate: "clamp"
        });

        const thisBar = (
          <View
            key={`bar${i}`}
            style={[
              styles.track,
              {
                width: this.itemWidth,
                marginLeft: i === 0 ? 0 : BAR_SPACE
              }
            ]}
          >
            <Animated.View
              style={[
                styles.bar,
                {
                  width: this.itemWidth,
                  transform: [{ translateX: scrollBarVal }]
                }
              ]}
            />
          </View>
        );
        barArray.push(thisBar);
      });

    return (
      <View style={styles.container} flex={1}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={10}
          pagingEnabled
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { x: this.animVal } } }
          ])}
        >
          {imageArray}
        </ScrollView>
        {this.props.logo ? (
          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={logo} />
          </View>
        ) : null}
        <View style={styles.barContainer}>{barArray}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 260,
    marginBottom: 20,
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  logoContainer: {
    position: "absolute",
    zIndex: 2,
    top: 40,
    right: 20,
    flexDirection: "row"
  },
  logo: {
    height: 50,
    width: 50
  },
  barContainer: {
    position: "absolute",
    zIndex: 2,
    top: 240,
    flexDirection: "row"
  },
  track: {
    backgroundColor: "#FFF",
    overflow: "hidden",
    height: 2
  },
  bar: {
    backgroundColor: "#F76D1E",
    height: 2,
    position: "absolute",
    left: 0,
    top: 0
  }
});

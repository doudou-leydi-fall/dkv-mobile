import React, { PureComponent } from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from "react-native";
import moment from "moment/min/moment-with-locales";
moment.locale("fr");

export class HeaderTop extends React.Component {
  constructor(props) {
    super(props);
    const date = new Date().toISOString();
    const hours = moment(date).format("HH:mm");
    this.state = {
      time: hours
    };
  }

  componentDidMount() {
    this.intervalID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  getHours() {
    const date = new Date().toISOString();
    return moment(date).format("HH:mm");
  }

  tick() {
    this.setState({
      time: this.getHours()
    });
  }

  render() {
    return (
      <View style={styles.headerContainer}>
        <Image
          source={require("../../assets/images/small-logo-inverse.png")}
          style={{ width: 50, height: 50 }}
        />
        <Text style={styles.label}>{this.state.time}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    height: 90,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 30,
    alignItems: "center",
    backgroundColor: "#010008"
  },
  label: {
    fontFamily: "trs-million",
    color: "#50E3C2",
    fontSize: 40,
    textAlign: "center"
  }
});

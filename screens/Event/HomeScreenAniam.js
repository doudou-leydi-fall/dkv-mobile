import React from "react";
import { InteractionManager, StyleSheet, Text, View } from "react-native";
import { SharedElementRenderer } from "react-native-motion";
import ToolbarBackground from "./_views/ToolbarBackground";
import HomeListScreen from "./HomeList";
import EventDetails from "./Details";

export default class EventHomeScree extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedItem: null,
      phase: "phase-0"
    };
  }

  onItemPressed = item => {
    this.setState({
      phase: "phase-1",
      selectedItem: item
    });
  };
  onBackPressed = () => {
    this.setState({
      phase: "phase-3"
    });
  };

  onSharedElementMovedToDestination = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        phase: "phase-2"
      });
    });
  };
  onSharedElementMovedToSource = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        selectedItem: null,
        phase: "phase-0"
      });
    });
  };

  renderPage() {
    const { selectedItem, position, detailItem, phase } = this.state;

    return (
      <View style={{ flex: 1 }}>
        <HomeListScreen
          selectedItem={selectedItem}
          onItemPress={this.onItemPressed}
          phase={phase}
        />
        <EventDetails
          phase={phase}
          selectedItem={selectedItem}
          onBackPress={this.onBackPressed}
          onSharedElementMovedToDestination={
            this.onSharedElementMovedToDestination
          }
          onSharedElementMovedToSource={this.onSharedElementMovedToSource}
        />
      </View>
    );
  }

  render() {
    const {
      selectedItem,
      goToDetail,
      position,
      detailItem,
      goBackRequested,
      phase
    } = this.state;

    return (
      <SharedElementRenderer>
        <View style={styles.container}>
          <ToolbarBackground
            isHidden={phase !== "phase-1" && phase !== "phase-2"}
          />
          {this.renderPage()}
        </View>
      </SharedElementRenderer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

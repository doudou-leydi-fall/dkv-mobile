const events = [
  {
    name: "Concert de Youssou",
    start: "2015-01-01T12:10:30Z",
    avatar:
      "https://xalimasn.com/wp-content/uploads/2018/06/10257393-16743230.jpg",
    place: {
      name: "Grand Théatre"
    }
  },
  {
    name: "Concert de Dip",
    start: "2015-01-01T12:10:30Z",
    avatar:
      "https://www.metrodakar.net/wp-content/uploads/2019/09/Dip-Doundou-Guiss-1.jpg",
    place: {
      name: "Place de la Nation"
    }
  },
  {
    name: "Sound System",
    start: "2015-01-01T12:10:30Z",
    avatar:
      "https://agdk.nelamservices.com/system/evenements/photos/000/007/590/cover/19944640_667495760110906_5568639689739389797_o.jpg?1500291123",
    place: {
      name: "Sharkys"
    }
  },
  {
    name: "Teaupla de Elzo",
    start: "2015-01-01T12:10:30Z",
    avatar: "https://i.ytimg.com/vi/2Q0K1FqetgY/maxresdefault.jpg",
    place: {
      name: "Plateau"
    }
  }
];

export default events;

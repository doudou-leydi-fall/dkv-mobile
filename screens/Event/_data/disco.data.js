const disco = [
  {
    name: "Castel Nigth Club",
    start: "2015-01-01T12:10:30Z",
    avatar:
      "https://1.bp.blogspot.com/-4paBx8uqqEg/XXuQ5h0bs5I/AAAAAAAACCg/pFgxliojeEcMW1q4jFyXLyGfaUHLuyEEQCLcBGAsYHQ/s640/Club-night-soiree-party-vacance-loisirs-sortie-LEUKSENEGAL-Dakar-Senegal-Afrique-Castel.jpg",
    place: {
      name: "Soirée platine"
    }
  },
  {
    name: "Night Club Gaindé",
    start: "2015-01-01T12:10:30Z",
    avatar: "https://i.ytimg.com/vi/PWwmJ9MnTyU/maxresdefault.jpg",
    place: {
      name: "Place de la Nation"
    }
  },
  {
    name: "Sound System",
    start: "2015-01-01T12:10:30Z",
    avatar:
      "https://agdk.nelamservices.com/system/evenements/photos/000/007/590/cover/19944640_667495760110906_5568639689739389797_o.jpg?1500291123",
    place: {
      name: "Sharkys"
    }
  },
  {
    name: "Teaupla de Elzo",
    start: "2015-01-01T12:10:30Z",
    avatar: "https://i.ytimg.com/vi/2Q0K1FqetgY/maxresdefault.jpg",
    place: {
      name: "Plateau"
    }
  }
];

export default disco;

const placesTop = [
  {
    name: "Sharkys",
    notation: 4.8,
    avatar:
      "https://s3-media0.fl.yelpcdn.com/bphoto/mQ4kaIpefJ2oy-OkpGFeYQ/o.jpg"
  },
  {
    name: "Marina B",
    notation: 4.7,
    avatar:
      "https://media-cdn.tripadvisor.com/media/photo-s/0b/8f/66/14/photo3jpg.jpg"
  },
  {
    name: "Pullman",
    notation: 4.6,
    avatar:
      "https://content.r9cdn.net/rimg/himg/dc/07/9b/ice-36146-62191757_3XL-611285.jpg?width=500&height=350&crop=true&caller=HotelDetailsPage3"
  },
  {
    name: "Radisson",
    notation: 4.5,
    avatar:
      "https://i.pinimg.com/originals/94/64/6e/94646e44bc3766a79729b94bf4e0f7ff.jpg"
  }
];

export default placesTop;

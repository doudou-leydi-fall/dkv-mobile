import React, { PureComponent } from "react";
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view'
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Easing
} from "react-native";
import {
  HeaderTop,
  WidthCard,
  Section,
  PictureSlider,
  CardList
} from "../../components";
import { SlimCardPlace } from "./_views/SlimCardPlace";
import EventCard from "./_views/EventCard";
import events from "./_data/events.data";
import placesTop from "./_data/places.top.data";
import disco from "./_data/disco.data";
import pictures from "./_data/pictures.data";
import Carousel from "./_views/Carousel";

const { width, height } = Dimensions.get("window");

const SecondRoute = () => <View style={[styles.container, { backgroundColor: '#673ab7' }]} />;

export default class HomeListScreen extends PureComponent {
  static navigationOptions = {
    header: props => <HeaderTop label={"23:30"} />
  };

  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'First' },
      { key: 'second', title: 'Second' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBar {...props} />;

  _renderScene = SceneMap({
    first: Home,
    second: SecondRoute,
  });

  render() {
    return (
      <TabViewAnimated
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
        initialLayout={initialLayout}
      />
    );
  }
}

const Home = () => (
  <ScrollView style={sectionStyle.container}>
    <Carousel pictures={pictures} />
    <Section
      title={"Ce soir"}
      items={events}
      childrenComponent={<EventCard />}
    />

    <Section
      title={"Tendances du moment"}
      items={placesTop}
      childrenComponent={<SlimCardPlace />}
    />

    <Section
      title={"Discothèques"}
      items={disco}
      childrenComponent={<EventCard />}
    />

    <Section
      title={"Discothèques"}
      items={disco}
      childrenComponent={<CardList />}
    />

    <View style={{ height: 80 }}></View>
  </ScrollView>
)

const sectionStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#211337",
    ...StyleSheet.absoluteFillObject
  },
  titleContainer: {
    flex: 1,
    backgroundColor: "#FFF",
    paddingTop: 20
  },
  title: {
    fontSize: 24,
    color: "#FFF",
    fontWeight: "700",
    paddingHorizontal: 20,
    paddingBottom: 20
  }
});

import React from "react";
import { SlimCard } from "../../../components";

export const SlimCardPlace = ({ data }) => (
  <SlimCard
    id={data.id}
    label={data.name}
    notation={data.notation}
    src={data.avatar}
  />
);

import React from "react";
import { WidthCard } from "../../../components";
import moment from "moment/min/moment-with-locales";
moment.locale("fr");

const EventCard = ({ data }) => (
  <WidthCard
    footerLB={data.place.name}
    footerLT={data.name}
    footerRB={moment(data.start).format("HH:mm")}
    footerRT={moment(data.start).format("dddd Do MMMM")}
    src={data.avatar}
  />
);

export default EventCard;

import React, { Component, PureComponent, Fragment } from "react";
import { View, Text, ScrollView, StyleSheet } from "react-native";
import { SharedElement, TranslateYAndOpacity } from "react-native-motion";
import events from "../_data/events.data";
import EventCard from "../_views/EventCard";
import { HeaderTop } from "../../../components";

export default class ListPage extends Component {
  static navigationOptions = {
    header: props => <HeaderTop label={"23:30"} />
  };
  render() {
    return (
      <Fragment>
        <SharedElement id="source">
          <List events={events} />
        </SharedElement>
        <SharedElement sourceId="source">
          <View>
            <Text>Hello</Text>
          </View>
        </SharedElement>
      </Fragment>
    );
  }
}

class ListScreen extends Component {
  render() {
    return (
      <SharedElement id="source">
        <List events={this.props.events} />
      </SharedElement>
    );
  }
}

const List = ({ events }) => {
  return (
    <ScrollView style={style.container}>
      {events.map((event, key) => (
        <EventCard sourceId={key} data={event} />
      ))}
    </ScrollView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#211337"
  }
});

class ToolbarTitle extends PureComponent {
  render() {
    return (
      <TranslateYAndOpacity duration={600}>
        <View>
          <Text>Hello</Text>
        </View>
      </TranslateYAndOpacity>
    );
  }
}

class DetailPage extends Component {
  render() {
    return (
      <SharedElement sourceId="source">
        <View>
          <Text>Hello</Text>
        </View>
      </SharedElement>
    );
  }
}

const sections = [
  {
    name: "Pizzas",
    data: [
      {
        name: "Pizza Orientale",
        price: 5000,
        currency: "FCFA",
        avatar:
          "https://lh4.googleusercontent.com/proxy/zAlKESsklXdLRGxysUX2zdfV6xRI5R76pKPZV0msWVSDudY1mMaKJrrThEfIxOHzLVJA9EwmIeiS1jjqDOCLWn0ZRB94BkU5MENnKX4-Slv_L3y0iIvIx7dd7vwss1Unxl5V62MsTAeV6yoxy5w6is7L00w3Fs8",
        recipes: ["Tomates", "Mergez", "Poivrons", "Mozzerella"]
      },
      {
        name: "Pizza 3 fromages",
        price: 2000,
        currency: "FCFA",
        avatar:
          "https://www.galbani.fr/wp-content/uploads/2017/07/veritable_pizza_4_fromages.jpg",
        recipes: ["Tomates", "Emmentale", "Chèvre", "Mozzerella"]
      },
      {
        name: "Pizza Chèvre miel",
        price: 2800,
        currency: "FCFA",
        avatar:
          "https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Ffac.2F2018.2F07.2F30.2Fe44346aa-308e-4a76-b40f-779d2447cd9b.2Ejpeg/748x372/quality/80/crop-from/center/pizza-au-magret-fume-chevre-et-miel.jpeg",
        recipes: ["Tomates", "Chèvre", "Miel", "Mozzerella"]
      },
      {
        name: "Pizza Campionne",
        price: 3800,
        currency: "FCFA",
        avatar:
          "https://www.galbani.fr/wp-content/uploads/2017/07/12cs_pizza_viande_hache_moza_guccina_38b9953.jpg",
        recipes: ["Tomates", "Viande hachée", "Poivron", "Mozzerella"]
      },
      {
        name: "Pizza Viande hachée",
        price: 3800,
        currency: "FCFA",
        avatar:
          "https://www.galbani.fr/wp-content/uploads/2017/07/12cs_pizza_viande_hache_moza_guccina_38b9953.jpg",
        recipes: ["Tomates", "Viande hachée", "Poivron", "Mozzerella"]
      },
      {
        name: "Pizza Atlantique",
        price: 3800,
        currency: "FCFA",
        avatar:
          "https://www.atelierdeschefs.com/media/recette-e23066-pizza-nordique.jpg",
        recipes: ["Tomates", "Saumon", "Pomme de terre", "Mozzerella"]
      }
    ]
  }
];

export default sections;

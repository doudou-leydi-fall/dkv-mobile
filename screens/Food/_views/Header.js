import React, { PureComponent } from "react";
import { Image, Text, View, StyleSheet } from "react-native";

export default class Header extends PureComponent {
  constructor(props) {
    super(props);

    console.log(this.props);
  }
  render() {
    console.log("Header props", this.props.title);
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.title}>{this.props.title || "sharkys"}</Text>
        <Image
          source={require("../../../assets/images/logo-official.png")}
          style={{ width: 50, height: 50 }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    height: 100,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    alignItems: "center",
    backgroundColor: "#FFF"
  },
  title: {
    color: "#170E5C",
    fontSize: 30,
    fontWeight: "700"
  }
});

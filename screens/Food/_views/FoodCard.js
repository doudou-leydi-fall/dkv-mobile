import React from "react";
import { StyleSheet } from "react-native";
import { WidthCard } from "../../../components";
import moment from "moment/min/moment-with-locales";
moment.locale("fr");

const FoodCard = ({ data }) => (
  <WidthCard
    footerRT={`${data.price} ${data.currency}`}
    footerLT={data.name}
    footerLB={data.recipes && data.recipes.join("-")}
    src={data.avatar}
    footerLTStyle={{
      fontSize: 17,
      fontWeight: "600",
      color: "#1B106C",
      marginBottom: 3
    }}
    footerLBStyle={{
      fontSize: 13,
      fontWeight: "400",
      color: "#1B106C",
      marginBottom: 3
    }}
    footerRTStyle={{
      fontSize: 16,
      fontWeight: "600",
      color: "#F76D1E",
      marginBottom: 3,
      textAlign: "right"
    }}
    footerRBStyle={{
      fontSize: 13,
      fontWeight: "600",
      color: "#1B106C",
      marginBottom: 3
    }}
  />
);

export default FoodCard;

import React from "react";
import { Carousel } from "../../../components";
import pictures from "../_data/pictures.data";
export default () => <Carousel pictures={pictures} />;

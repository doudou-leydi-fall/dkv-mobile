import React, { PureComponent } from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from "react-native";
import Carousel from "./_views/Carousel";
import Header from "./_views/Header";
import pictures from "./_data/pictures.data";
import { Section } from "../../components";
import FoodCard from "./_views/FoodCard";
import sections from "./_data/sections.data";
import events from "../Event/_data/events.data";

const { width, height } = Dimensions.get("window");

export default class PlaceFoodScreen extends PureComponent {
  static navigationOptions = {
    header: props => <Header title={"Sharkys"} />
  };
  render() {
    console.log("sections LOG", sections[0].data);
    return (
      <ScrollView style={styles.container}>
        <Carousel pictures={pictures} />
        <Section
          title={sections[0].name}
          items={sections[0].data}
          childrenComponent={<FoodCard />}
          titleStyle={{
            color: "#1B106C",
            fontSize: 25,
            fontWeight: "800"
          }}
        />
        <Section
          title={sections[0].name}
          items={sections[0].data}
          childrenComponent={<FoodCard />}
          titleStyle={{
            color: "#1B106C",
            fontSize: 25,
            fontWeight: "800"
          }}
        />
        <Section
          title={sections[0].name}
          items={sections[0].data}
          childrenComponent={<FoodCard />}
          titleStyle={{
            color: "#1B106C",
            fontSize: 25,
            fontWeight: "800"
          }}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    height: 100,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    alignItems: "center",
    backgroundColor: "#FFF"
  },
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  titleContainer: {
    flex: 1,
    backgroundColor: "#FFF",
    paddingTop: 20
  },
  title: {
    fontSize: 24,
    color: "#FFF",
    fontWeight: "700",
    paddingHorizontal: 20,
    paddingBottom: 20
  }
});
